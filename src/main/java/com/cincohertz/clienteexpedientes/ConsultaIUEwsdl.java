/**
 * ConsultaIUEwsdl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.cincohertz.clienteexpedientes;

public interface ConsultaIUEwsdl extends javax.xml.rpc.Service {
    public java.lang.String getconsultaIUEwsdlPortAddress();

    public com.cincohertz.clienteexpedientes.ConsultaIUEwsdlPortType getconsultaIUEwsdlPort() throws javax.xml.rpc.ServiceException;

    public com.cincohertz.clienteexpedientes.ConsultaIUEwsdlPortType getconsultaIUEwsdlPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
