<?xml version="1.0" encoding="ISO-8859-1"?>
<definitions xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" xmlns:tns="urn:consultaIUEwsdl" xmlns:soap="http://schemas.xmlsoap.org/wsdl/soap/" xmlns:wsdl="http://schemas.xmlsoap.org/wsdl/" xmlns="http://schemas.xmlsoap.org/wsdl/" targetNamespace="urn:consultaIUEwsdl">
<types>
<xsd:schema targetNamespace="urn:consultaIUEwsdl"
>
 <xsd:import namespace="http://schemas.xmlsoap.org/soap/encoding/" />
 <xsd:import namespace="http://schemas.xmlsoap.org/wsdl/" />
 <xsd:complexType name="giro">
  <xsd:all>
   <xsd:element name="fecha" type="xsd:string"/>
   <xsd:element name="tipo" type="xsd:string"/>
   <xsd:element name="decreto" type="xsd:string"/>
   <xsd:element name="vencimiento" type="xsd:string"/>
   <xsd:element name="sede" type="xsd:string"/>
  </xsd:all>
 </xsd:complexType>
 <xsd:complexType name="giros">
  <xsd:complexContent>
   <xsd:restriction base="SOAP-ENC:Array">
    <xsd:attribute ref="SOAP-ENC:arrayType" wsdl:arrayType="tns:giro[]"/>
   </xsd:restriction>
  </xsd:complexContent>
 </xsd:complexType>
 <xsd:complexType name="resultado">
  <xsd:all>
   <xsd:element name="estado" type="xsd:string"/>
   <xsd:element name="origen" type="xsd:string"/>
   <xsd:element name="expediente" type="xsd:string"/>
   <xsd:element name="caratula" type="xsd:string"/>
   <xsd:element name="abogado_actor" type="xsd:string"/>
   <xsd:element name="abogado_demandante" type="xsd:string"/>
   <xsd:element name="movimientos" type="tns:giros"/>
  </xsd:all>
 </xsd:complexType>
</xsd:schema>
</types>
<message name="consultaIUERequest">
  <part name="iue" type="xsd:string" /></message>
<message name="consultaIUEResponse">
  <part name="resultado" type="tns:resultado" /></message>
<portType name="consultaIUEwsdlPortType">
  <operation name="consultaIUE">
    <documentation>Dada una iue devuelve los datos de la sede</documentation>
    <input message="tns:consultaIUERequest"/>
    <output message="tns:consultaIUEResponse"/>
  </operation>
</portType>
<binding name="consultaIUEwsdlBinding" type="tns:consultaIUEwsdlPortType">
  <soap:binding style="rpc" transport="http://schemas.xmlsoap.org/soap/http"/>
  <operation name="consultaIUE">
    <soap:operation soapAction="urn:consultaIUEwsdl#consultaIUE" style="rpc"/>
    <input><soap:body use="encoded" namespace="urn:consultaIUEwsdl" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></input>
    <output><soap:body use="encoded" namespace="urn:consultaIUEwsdl" encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"/></output>
  </operation>
</binding>
<service name="consultaIUEwsdl">
  <port name="consultaIUEwsdlPort" binding="tns:consultaIUEwsdlBinding">
    <soap:address location="http://expedientes.poderjudicial.gub.uy/wsConsultaIUE.php"/>
  </port>
</service>
</definitions>