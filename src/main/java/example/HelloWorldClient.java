package example;


import com.cincohertz.clienteexpedientes.ConsultaIUEwsdlLocator;
import com.cincohertz.clienteexpedientes.ConsultaIUEwsdlPortType;
import com.cincohertz.clienteexpedientes.Resultado;

public class HelloWorldClient {
  public static void main(String[] argv) {
      try {
          ConsultaIUEwsdlLocator locator = new ConsultaIUEwsdlLocator();

          ConsultaIUEwsdlPortType consultaIUEwsdlPortType = locator.getconsultaIUEwsdlPort();
          // If authorization is required
          //((ConsultaIUEwsdlBindingStub)service).setUsername("user3");
          //((ConsultaIUEwsdlBindingStub)service).setPassword("pass3");
          // invoke business method
          Resultado resultado = consultaIUEwsdlPortType.consultaIUE("2-1/2018");
          System.out.println(resultado.getCaratula());
      } catch (javax.xml.rpc.ServiceException ex) {
          ex.printStackTrace();
      } catch (java.rmi.RemoteException ex) {
          ex.printStackTrace();
      }
  }
}
